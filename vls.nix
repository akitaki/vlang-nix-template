{ pkgs ? import <nixpkgs> {}, vlang }:

pkgs.stdenv.mkDerivation rec {
  name = "vls";
  version = "eee1727959647904091afd41cb94a717719d3301";
  vershort = "eee1727";

  src = pkgs.fetchFromGitHub {
    owner = "vlang";
    repo = "vls";
    rev = version;
    sha256 = "018nykfdykjpy53nccr4f4j7viy6611f8bcv80nnz514nh4agcpl";
  };
  buildInputs = [ vlang ];
  nativeBuildInputs = [ vlang pkgs.gcc pkgs.boehmgc pkgs.makeWrapper ];
  postPatch = ''
    substituteInPlace build.vsh \
      --replace "git rev-parse --short HEAD" "echo ${vershort}"
  '';
  buildPhase = ''
    export HOME=$TMP
    ${vlang}/bin/v run build.vsh gcc
  '';
  installPhase = ''
    echo "vlang=" ${vlang}
    mkdir -p $out/bin
    cp bin/vls $out/bin/vls
  '';
}
