#!/bin/sh
# requires nix-prefetch to be installed

get_latest_release() {
    curl --silent "https://api.github.com/repos/$1/releases/latest" |
        grep '"tag_name":' |
        sed -E 's/.*"([^"]+)".*/\1/'
}
prefetch() {
    echo "$1/$2:"
    2>/dev/null nix-prefetch fetchFromGitHub --owner $1 --repo $2 $3 --output nix
}
prefetch vlang v "--rev $(get_latest_release vlang/v)"
prefetch vlang vc
prefetch vlang markdown
prefetch vlang vls

