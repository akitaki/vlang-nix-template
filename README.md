# vlang-nix-template

A project template for development with [vlang](https://vlang.io/) with Nix.
Credits to [Artturin's original nix derivation](https://github.com/Artturin/nixpkgs/blob/6b1ab33dad970032d282f969dd5343dbc109832f/pkgs/development/compilers/vlang/default.nix).

To use the template, just clone it and run `nix-shell`.
`v` and `vls` are available.
