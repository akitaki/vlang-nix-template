{ pkgs ? import <nixpkgs> {} }:

let
  lib = pkgs.lib;
  stdenv = pkgs.stdenv;
  fetchFromGitHub = pkgs.fetchFromGitHub;
  glfw = pkgs.glfw;
  freetype = pkgs.freetype;
  openssl = pkgs.openssl;
  makeWrapper = pkgs.makeWrapper;
  upx = pkgs.upx;
  git = pkgs.git;
  strace = pkgs.strace;
  binutils-unwrapped = pkgs.binutils-unwrapped;
  libGL = pkgs.libGL;
  boehmgc = pkgs.boehmgc;
  sqlite = pkgs.sqlite;
  libexecinfo = pkgs.libexecinfo;
  xorg = pkgs.xorg;
  valgrind = pkgs.valgrind;
in
  stdenv.mkDerivation rec {
    pname = "vlang";
    version = "weekly.2021.44";
  
    src = pkgs.fetchFromGitHub {
      owner = "vlang";
      repo = "v";
      rev = version;
      sha256 = "0rb69sv1mrwz2115mzp3mlb83bmm4r6l6nlfkm00k67gv8a7cb46";
    };
  
    # V compiler source translated to C for bootstrap.
    vc = fetchFromGitHub {
      owner = "vlang";
      repo = "vc";
      rev = "5e563c318bdf38225de4d0b4683fc13c6cf6a885";
      sha256 = "0x96wsgsw4q4hkfklagqccl4jqxw3gbnjd5gif1pj50apclc705l";
    };
  
    # https://github.com/vlang/markdown
    markdown = fetchFromGitHub {
      owner = "vlang";
      repo = "markdown";
      rev = "1cda5d4a1538cac51504651cb5a4e02406807452";
      sha256 = "0g3r3z2q4ncg3lyi2wl7h8l025zr21j5jw72hq1v94645n6vx5i3";
    };
  
    nativeBuildInputs = [
      makeWrapper
      git
      strace
      libGL
      openssl
      boehmgc
      sqlite
      libexecinfo
      xorg.libX11.dev
      xorg.libX11
      xorg.xinput
      xorg.libXi
      xorg.libXext
      xorg.libXcursor
      valgrind
      pkgs.tmux
    ];
  
    propagatedBuildInputs = [
      glfw
      freetype
      openssl
    ] ++ lib.optional stdenv.hostPlatform.isUnix upx;
  
    postPatch = ''
      substituteInPlace Makefile \
        --replace "rm -rf" "true" \
        --replace "git" "echo"
      substituteInPlace vlib/v/util/util.v \
        --replace "https://github.com/vlang" "gits" \
        --replace "git clone" "cp -r --no-preserve=mode"
    '';
  
    preBuild = ''
      export HOME=$PWD
      mkdir gits
      cp -r --no-preserve=mode $markdown gits/markdown
      cp -r --no-preserve=mode $vc vc
      export VERBOSE=1
      export VCREPO=vc
      export TCCREPO=
      export local=true
      export VFLAGS=-g
      makeFlagsArray+=(CFLAGS="-O0")
    '';
  
    postBuild = ''
      # Exclude thirdparty/vschannel as it is windows-specific.
      #filelist=$(find cmd -type f -name '*.v' | sed 's|cmd/v/help/help.v||; s|cmd/tools/vdoc/markdown.v||; s|cmd/tools/vdoc/vdoc.v||')
      rm -rf thirdparty/vschannel thirdparty/picoev
      find thirdparty -type f -name "*.c" -execdir cc -std=gnu11 $CFLAGS -w -c {} $LDFLAGS ';'
  
      tmux new-session -d -s task-session -x 80 -y 60
      tmux send-keys './v build-tools -v' ENTER
      tmux send-keys 'tmux wait -S ping' ENTER
      tmux send-keys 'exit' ENTER
      tmux wait ping
    '';
  
    libPath = lib.makeLibraryPath (with pkgs; [
      libGL
      openssl
      boehmgc
      sqlite
      libexecinfo
      xorg.libX11.dev
      xorg.libX11
      xorg.xinput
      xorg.libXi
      xorg.libXext
      xorg.libXcursor
      bintools-unwrapped
      valgrind
    ]);
  
    installPhase = ''
      runHook preInstall
      mkdir -p $out/{bin,lib,share}
      cp -r examples $out/share
      cp -r {cmd,vlib,thirdparty} $out/lib
      mv v $out/lib
      echo 'making wrapper'
      ln -s $out/lib/v $out/bin/v
      wrapProgram $out/bin/v --prefix PATH : ${lib.makeBinPath [ stdenv.cc binutils-unwrapped ]} --prefix LD_LIBRARY_PATH : ${libPath}
      runHook postInstall
    '';
  
    meta = with lib; {
      homepage = "https://vlang.io/";
      description = "Simple, fast, safe, compiled language for developing maintainable software";
      license = licenses.mit;
      maintainers = with maintainers; [ chiiruno ];
      platforms = platforms.all;
    };
  }
